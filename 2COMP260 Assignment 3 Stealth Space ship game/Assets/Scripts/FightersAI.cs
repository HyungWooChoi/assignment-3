﻿using UnityEngine;
using System.Collections;

public class FightersAI : MonoBehaviour {
	public DetailsManager playerDetails;

	public Transform[] patrolPoints;

	public float MinimumDistance;
	public float speed;

	int i = 1;
	Rigidbody rig;
	// Use this for initialization
	void Start () {
		playerDetails = GameObject.Find("Details").GetComponent<DetailsManager>();
		i = 1;
		rig = this.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
//		print ("i = "+i);

		if (i < patrolPoints.Length)
		{
//			print ("TestB");
			transform.LookAt(new Vector3(patrolPoints[i].transform.position.x,patrolPoints[i].transform.position.y,this.transform.position.z));

			float Distance = Vector2.Distance (patrolPoints[i].transform.position, new Vector2 (this.transform.position.x, this.transform.position.y));
//			print ("Distance is: "+Distance);
			if (Distance > MinimumDistance)
			{
//				print ("TestC");
				if (rig.velocity.sqrMagnitude < speed)
				{
//					print ("TestD");
					rig.AddForce (transform.forward * speed * Time.deltaTime, ForceMode.Acceleration);
				}
			}
			else if (Distance < MinimumDistance)
			{
//				print ("TestE");
				i++;
			}
		}
		else
		{
			i = 0;
		}
	}
}
