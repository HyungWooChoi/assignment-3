﻿using UnityEngine;
using System.Collections;

public class SensorZones : MonoBehaviour {
	public DetailsManager playerSighted;

	public GameObject target;

	public bool forGun;
	public GameObject[] Turrets;

	public bool forSensor;
	public GameObject Fighters;
	public Transform[] SpawnPoint;

	public float startTime;
	public float Count;
	public bool FinishedCounting;

	public int spawnedFighterCount;

	// Use this for initialization
	void Start () {
		Count = startTime;
		FinishedCounting = false;
		spawnedFighterCount = 0;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
//		playerSighted = GameObject.Find("Details").GetComponent<DetailsManager>();
	
	}

	void OnTriggerEnter(Collider col)
	{
		playerSighted = GameObject.Find("Details").GetComponent<DetailsManager>();
		if(col.tag == "Player")
		{
			target = col.gameObject;
			//playerSighted.playerFound = true;
		}
	}

	void OnTriggerStay(Collider col)
	{
		if(col.gameObject.tag == "Player")
		{
			if(FinishedCounting == false)
			{
				Count -= 1 * Time.deltaTime;
			}
		}
		if(Count <=0)
		{
//			print ("Testing");
			playerSighted.playerFound = true;
			if(forGun)
			{
				for(int i =0; i<Turrets.Length;i++)
				{
					Turrets [i].transform.LookAt (new Vector3(target.transform.position.x,target.transform.position.y,Turrets[i].transform.position.z));
					PlayerLife player = col.GetComponent<PlayerLife>();
					player.playerisDead = true;
				}
			}
			FinishedCounting = true;
			if (forSensor) {
				if (spawnedFighterCount < SpawnPoint.Length) {
					for (int i = 0; i < SpawnPoint.Length; i++) {
						GameObject Fighter = Instantiate (Fighters, SpawnPoint [i].transform.position, SpawnPoint [i].transform.rotation) as GameObject;
						spawnedFighterCount++;
					}
				}
			}
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Player")
		{
			playerSighted.playerFound = false;
		}
	}
}
