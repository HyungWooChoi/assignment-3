﻿using UnityEngine;
using System.Collections;

public class Torps : MonoBehaviour {

	public bool isTorp;

	public GameObject explosion;

	public float speed;
	public Transform target;
	public Vector3 endgoal;
	float Distance;

	public Rigidbody rig;
	// Use this for initialization
	void Start () {
		rig = GetComponent<Rigidbody> ();
		if(isTorp)
		{
			rig.AddForce (transform.up*speed,ForceMode.Impulse);
		}
		else
		{
			rig.AddForce (transform.forward*speed,ForceMode.Impulse);
		}

		target = GameObject.Find("Player").transform;
		endgoal = target.transform.position;
		if(!isTorp)
		{
			transform.LookAt (target);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!isTorp)
		{
			Distance = Vector3.Distance (endgoal, transform.position);
//			print ("Distance is: "+Distance);
			if (Distance < 1)
			{
				Explode ();
			}
		}
	}

	void OnCollisionEnter(Collision col)
	{
		if(isTorp)
		{
			Explode ();
		}
	}

	public void Explode()
	{
		Instantiate (explosion,transform.position,transform.rotation);
		Destroy (this.gameObject);
	}
}
