﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DetailsManager : MonoBehaviour {
	public PlayerLife player;

//	public Vector2 lastFoundPos;

	public bool playerFound;

	public int restartTime;
	public float restartTimer;

	public int numberofGoalsNeeded;

	public int count;
	public Text Finaltext;

	public static DetailsManager manager;

	void Awake()
	{
//		Finaltext = GameObject.Find ("FinalText");
		playerFound = false;
		Object.DontDestroyOnLoad (this.gameObject);
		if (manager == null) {
			manager = this;
		}
		else
		{
			DestroyObject (gameObject);
		}
	}
	// Use this for initialization
	void Start () {
		count = 0;
		restartTimer = restartTime;

	}

	// Update is called once per frame
	void Update () {
		if(count <numberofGoalsNeeded)
		{
			Finaltext = GameObject.FindGameObjectWithTag("Text").GetComponent<Text>();
			Finaltext.text = "";
		}
		Object.DontDestroyOnLoad (this.gameObject);

		player = FindObjectOfType (typeof(PlayerLife))as PlayerLife;

		if(player.playerisDead == true)
		{
			restartTimer -= 1 * Time.deltaTime;
			if(restartTimer <=0)
			{
                count = 0;
                Application.LoadLevel(Application.loadedLevel);
				restartTimer = restartTime;
			}
		}
		if(count >= numberofGoalsNeeded)
		{
			Finaltext.text = "MISSION  " +
				"COMPLETE";
			restartTimer -= 1 * Time.deltaTime;
			if (restartTimer <= 0)
			{

				count = 0;
				Application.LoadLevel (Application.loadedLevel);
				restartTimer = restartTime;
			}
		}

//		if(playerFound)
//		{
//			lastFoundPos = player.gameObject.transform.position;
//		}
	}
}
