﻿using UnityEngine;
using System.Collections;

public class cameraFollow : MonoBehaviour {

	public Transform target;

	// Use this for initialization
	void Start () {
		this.transform.Translate (0,0,-16);
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = new Vector3 (target.transform.position.x,target.transform.position.y,-16);
	}
}
