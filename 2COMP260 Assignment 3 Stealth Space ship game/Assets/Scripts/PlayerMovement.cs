﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour {

	public float speed = 10f;
	public float turnSpeed = 5f;
	public float speedLimit = 10f;

	public KeyCode Forward;
	public KeyCode Right;
	public KeyCode Left;
	public KeyCode Backwards;

	public Rigidbody riggedbod;

	public ParticleSystem[] effects;
	// Use this for initialization
	void Start () {
		riggedbod = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		playerInput ();
	}
//	void FixedUpdate()
//	{
//		print (riggedbod.velocity.sqrMagnitude);
//	}

	public void playerInput()
	{
		if(riggedbod.velocity.sqrMagnitude <speedLimit)
		{
			if (Input.GetKey (Forward)) {
				effects [0].Emit(10);
				effects [1].Emit(10);
				effects [2].Emit(10);
				riggedbod.AddForce (transform.up * speed, ForceMode.Acceleration);
			} else {
				effects [0].Emit(0);
				effects [1].Emit(0);
				effects [2].Emit(0);
			}
		}
		if(riggedbod.velocity.sqrMagnitude<speedLimit/3)
		{
			if (Input.GetKey (Backwards)) {
				effects [3].Emit(10);
				effects [4].Emit(10);
				riggedbod.AddForce (-transform.up * speed / 5, ForceMode.Acceleration);
			} else {
				effects [3].Emit(0);
				effects [4].Emit(0);
			}
		}
		if (Input.GetKey (Right)) {
			effects [5].Emit(10);
			transform.Rotate (-transform.forward * turnSpeed * Time.deltaTime);
		} else {
			effects [5].Emit(0);
		}
		if (Input.GetKey (Left)) {
			effects [6].Emit(10);
			transform.Rotate (transform.forward * turnSpeed * Time.deltaTime);
		} else {
			effects [6].Emit(0);
		}
	}
}
