﻿using UnityEngine;
using System.Collections;

public class MissileSpawner : MonoBehaviour {

	public bool isTorp;
//	public Transform TorpSpawnPoint;
	public GameObject Torp;

	public float delayTimer;
	public float SpawnTime;
	public float counter;
	public Transform[] MissilespawnPoints;
	public GameObject Missile;

	public DetailsManager manager;

	// Use this for initialization
	void Start () {
		if(!isTorp)
		{
			counter = SpawnTime;
		}
		else
		{
			counter = SpawnTime+delayTimer;
		}
		manager = GameObject.Find("Details").GetComponent<DetailsManager>();
		for(int i =0; i< MissilespawnPoints.Length;i++)
		{
			MissilespawnPoints[i].transform.LookAt (this.transform.position);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!isTorp)
		{
			if (manager.playerFound == true)
			{
				counter -= 1 * Time.deltaTime;
				if (counter <= 0)
				{
					
					int i = Random.Range (0, MissilespawnPoints.Length - 1);
					GameObject Danger = Instantiate (Missile, MissilespawnPoints [i].transform.position,MissilespawnPoints[i].transform.rotation)as GameObject;
					counter = SpawnTime;
				}
			}
			else
			{
				counter = SpawnTime;
			}
		}
		else
		{
			counter -= 1 * Time.deltaTime;
			if (counter <= 0)
			{
				GameObject EnvironmentalDanger = Instantiate (Torp,transform.position,transform.rotation) as GameObject;
				counter = SpawnTime;
			}
		}
	}
}
