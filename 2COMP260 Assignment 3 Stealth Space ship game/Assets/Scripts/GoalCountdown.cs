﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoalCountdown : MonoBehaviour {

	public DetailsManager manager;

	public Slider CounterBar;
	public Image sliderFill;

	public float Counter;
	public int countAdd;
	public int countEnd;

	public bool countFinished = false;

	int i;
	// Use this for initialization
	void Start () {
		CounterBar.maxValue = countEnd;
		manager = GameObject.Find("Details").GetComponent<DetailsManager>();
		i = 1;
	}
	
	// Update is called once per frame
	void Update () {

		CounterBar.value = Counter;
		if(Counter >= countEnd)
		{
//			print ("Point taken");
			countFinished = true;
			;
			if (i > 0)
			{
				manager.count++;
				i--;
			}
		}
	}

	void OnTriggerStay(Collider col)
	{
		if(countFinished == false)
		{
			if(col.tag == "Player")
			{
				Counter += countAdd*Time.deltaTime;
				byte red = 255;
				red = (byte)(red - (CounterBar.value*25.5));
				byte green = 1;
				green = (byte)(green + (CounterBar.value*25.5));
//				byte blue = (byte)(100-(int)CounterBar.value*2);

				sliderFill.GetComponent<Image> ().color = new Color32 (red,green,000,255);

			}
		}
	}
}
