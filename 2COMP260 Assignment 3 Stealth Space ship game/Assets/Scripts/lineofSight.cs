﻿using UnityEngine;
using System.Collections;

public class lineofSight : MonoBehaviour {
	public bool isFighter;

	public float fieldofView;

	public DetailsManager playerSighted;

	public Transform player;

	public float startTimer;
	public float count;

	float angle;
    Vector2 directions;
	// Use this for initialization
	void Start () {
		count = startTimer;
		playerSighted = GameObject.Find("Details").GetComponent<DetailsManager>();
		player = GameObject.Find ("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
//		playerSighted = GameObject.Find("Details").GetComponent<DetailsManager>();
		directions = player.transform.position - transform.position;
		if (!isFighter)
		{
			angle = Vector2.Angle (directions, transform.up);
		}
        if (!isFighter)
        {
            if (angle <= fieldofView / 2)
            {
                RaycastHit hit;
                Debug.DrawRay(transform.position, directions.normalized, Color.green);
                if (Physics.Raycast(transform.position, directions.normalized, out hit))
                {
                    if (hit.collider.gameObject.tag == "Player")
                    {
                        count -= 2 * Time.deltaTime;
                        if (count <= 0)
                        {
                            //						print ("プレイア発見。");
                            count = 0;
                            playerSighted.playerFound = true;

                            if (isFighter)
                            {
                                PlayerLife playerLife = hit.collider.GetComponent<PlayerLife>();
                                playerLife.playerisDead = true;
                            }
                        }

                        //					print ("player Found");
                    }
                    else
                    {
                        playerSighted.playerFound = false;
                    }
                }
            }
            else
            {
                if (count < startTimer)
                {
                    count += 0.5f * Time.deltaTime;
                }
                else if (count > startTimer)
                {
                    count = startTimer;
                }
                playerSighted.playerFound = false;
            }
        }
//		if (!playerSighted.playerFound)
//		{
//			if(count <startTimer)
//			{
//				count += 0.5f* Time.deltaTime;
//			}
//			else if(count > startTimer)
//			{
//				count = startTimer;
//			}
//		}
	}
    void OnTriggerStay(Collider col)
    {
        if (isFighter)
        {
            if (col.tag == "Player")
            {
                angle = Vector2.Angle(directions, transform.forward);
                RaycastHit hit;
                Debug.DrawRay(transform.position, directions.normalized, Color.green);
                if (Physics.Raycast(transform.position, directions.normalized, out hit))
                {
                    if (hit.collider.gameObject.tag == "Player")
                    {
                        count -= 2 * Time.deltaTime;
                        if (count <= 0)
                        {
                            //						print ("プレイア発見。");
                            count = 0;
                            playerSighted.playerFound = true;

                            if (isFighter)
                            {
                                PlayerLife playerLife = hit.collider.GetComponent<PlayerLife>();
                                playerLife.playerisDead = true;
                            }
                        }

                        //					print ("player Found");
                    }
                    else
                    {
                        playerSighted.playerFound = false;
                    }
                }
            }
            else
            {
                if (count < startTimer)
                {
                    count += 0.5f * Time.deltaTime;
                }
                else if (count > startTimer)
                {
                    count = startTimer;
                }
                playerSighted.playerFound = false;
            }
        }
    }
 
}
