﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {
	public float Counter;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Counter -= 1 * Time.deltaTime;
		if(Counter <=0)
		{
			Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "Player")
		{
			PlayerLife isDead = col.GetComponent<PlayerLife>();
			isDead.playerisDead = true;
		}
	}
}
