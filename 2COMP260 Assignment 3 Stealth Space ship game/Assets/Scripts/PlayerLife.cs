﻿using UnityEngine;
using System.Collections;

public class PlayerLife : MonoBehaviour {
	PlayerMovement move;
	CapsuleCollider hitBox;
	public GameObject looks;

	public bool playerisDead;

	public GameObject explosion;
	public bool playonce;
	void Awake()
	{
		move = this.GetComponent<PlayerMovement>();
		move.enabled = true;

		hitBox = this.GetComponent<CapsuleCollider>();
		hitBox.enabled = true;

//		looks = looks.GetComponent<MeshRenderer>();
//		looks.enabled = true;
	}

	// Use this for initialization
	void Start () {
		playerisDead = false;
		playonce = true;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if(playerisDead == true)
		{
			move.riggedbod.velocity = Vector3.zero;
			move.enabled = false;
			hitBox.enabled = false;
//			looks.enabled = false;
			Destroy(looks);
			if(playonce ==true)
			{
				GameObject boom = Instantiate (explosion,transform.position,transform.rotation) as GameObject;
				playonce = false;
			}
		}
	}


}
